import Control.Monad

powersGame :: Int -> String
powersGame n | (n `mod` 8) == 0 = "Second"
             | otherwise        = "First"

main = do
    t <- readLn :: IO Int
    forM_ [1..t] $ \tItr -> do
        n <- readLn :: IO Int
        putStrLn $ powersGame n
