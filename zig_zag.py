def convert(s, numRows):
    if numRows == 1 : return s
    period = 2*(numRows -1)
    d = {i:i if i<numRows else (period-i) for i in range(period)}
    lines = ["" for i in range(numRows)]
    for i in range(len(s)):
        lines[d[i%period]] += s[i]
    return "".join(lines)

print(convert('PAYPALISHIRING',3)) 
