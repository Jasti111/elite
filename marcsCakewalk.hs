import Data.List
import Data.Maybe

marcsCakewalk :: [Int] -> Int
marcsCakewalk arr = sum [ x * (2 ^ fromJust (elemIndex x arr)) | x <- arr]

main = do
    n <- readLn :: IO Int
    calorieTemp <- getLine
    let calorie = Data.List.map (read :: String -> Int) . words $ calorieTemp
    print $  marcsCakewalk (reverse (sort calorie))



--40
--819 701 578 403 50 400 983 665 510 523 696 532 51 449 333 234 958 460 277 347 950 53 123 227 646 190 938 61 409 110 61 178 659 989 625 237 944 550 954 439
--expected output : 59715404338867
--output : 57619460298419