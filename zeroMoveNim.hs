{-# LANGUAGE FlexibleInstances, UndecidableInstances, DuplicateRecordFields #-}

module Main where

import Control.Monad
import Data.List
import Data.List.Split
import Data.Bits

readMulLin :: Int -> IO [String]
readMulLin 0 = return []
readMulLin n = do
    line <- getLine
    rest <- readMulLin(n - 1)
    return (line : rest)

myxor :: Int -> Int -> Int
myxor a b = a `xor` b

movesNum :: [Int] -> Int
movesNum arr = foldl (myxor) 0 arr 

winZeroNim :: Int -> String
winZeroNim n | n == 0 = "L"
             | otherwise = "W"

zeroMoveNim :: [Int] -> [Int]
zeroMoveNim arr = [x -1 + 2 *(x `mod` 2) | x <- arr]

main = do
    n <- readLn :: IO Int
    forM_ [1..n] $ \n_itr -> do
        arrCount <- readLn :: IO Int
        arrTemp <- getLine
        let arr = Data.List.map (read :: String -> Int) . words $ arrTemp
        putStrLn $ winZeroNim $ movesNum $ zeroMoveNim arr 
