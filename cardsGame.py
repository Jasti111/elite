import random
cardsList = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
(random.shuffle(cardsList))
userCardsDict = {}
compCardsDict = {'2' : 2, '3' : 3, '4' : 4, '5' : 5, '6' : 6, '7' : 7, '8' : 8, '9' : 9, '10' : 10, 'J' : 11,'Q' : 12, 'K' : 13, 'A' : 14}
cardsDict = {'2' : 2, '3' : 3, '4' : 4, '5' : 5, '6' : 6, '7' : 7, '8' : 8, '9' : 9, '10' : 10, 'J' : 11,'Q' : 12, 'K' : 13, 'A' : 14}
comp = (compInput('9', compCardsDict, userCardsDict))

def compInput(card, compCardsDict, userCardsDict):
    if card == 'A':
        bid = '2'
    elif card == 'K':
        bid = 'A'
    elif card == '2':
      if '3' in compCardsDict.keys():
            bid = '3'
            compCardsDict.pop('3')
      else:
            keyList = list(compCardsDict.keys())
            valueList = list(compCardsDict.values())
            bid = keyList[valueList.index(min(compCardsDict.values()))]
            compCardsDict.pop(bid)

    elif card > '2' and card <= '8':
        #print(type(card), compCardsDict[card])
        if str(int(card) + 1) in compCardsDict.keys():
            bid = str(int(card) + 1)
            compCardsDict.pop(bid)
        else:
            keyList = list(compCardsDict.keys())
            valueList = list(compCardsDict.values())
            bid = keyList[valueList.index(min(compCardsDict.values()))]
            compCardsDict.pop(bid)
    else:
        userDict = {key:val for key, val in userCardsDict.items() if val != 'K' or val != 'A'}
        if userDict != {} and max(userDict.values()) >= max(compCardsDict.values()):
            keyList = list(compCardsDict.keys())
            valueList = list(compCardsDict.values())
            bid = keyList[valueList.index(min(compCardsDict.values()))]
            compCardsDict.pop(bid)
        else:
            keyList = list(compCardsDict.keys())
            valueList = list(compCardsDict.values())
            bid = keyList[valueList.index(max(compCardsDict.values()))]
            compCardsDict.pop(bid)
    print('My bid is ',bid)
    return compCardsDict, bid


import random
cardsList = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
random.shuffle(cardsList)

def diamondGame(cardsList):
    userDiamonds = 0
    compDiamonds = 0
    i = 0
    cardsDict = {'2' : 2, '3' : 3, '4' : 4, '5' : 5, '6' : 6, '7' : 7, '8' : 8, '9' : 9, '10' : 10, 'J' : 11, 'Q' : 12,  'K' : 13, 'A' : 14}
    userCardsDict = {'2' : 2, '3' : 3, '4' : 4, '5' : 5, '6' : 6, '7' : 7, '8' : 8, '9' : 9, '10' : 10, 'J' : 11, 'Q' : 12,  'K' : 13, 'A' : 14}
    compCardsDict = {'3' : 3, '4' : 4, '5' : 5, '6' : 6, '7' : 7, '8' : 8, '9' : 9, '10' : 10, 'J' : 11, 'Q' : 12, 'K' : 13}
    while any(userCardsDict):
        diamonds = cardsDict[cardsList[i]]
        print('The number of diamonds are ', diamonds)
        userTupple = userInput(userCardsDict)
        compTupple = compInput(cardsList[i], compCardsDict, userCardsDict)
        #print(userTupple, compTupple)
        #i += 1
        if cardsDict[userTupple[1]] > cardsDict[compTupple[1]]:
          userDiamonds += diamonds
        elif cardsDict[userTupple[1]] < cardsDict[compTupple[1]]:
          compDiamonds += diamonds
        else:
          compDiamonds += (diamonds / 2)
          userDiamonds += (diamonds / 2)
        i += 1
    print(userDiamonds, compDiamonds)
    return userDiamonds > compDiamonds
if diamondGame(cardsList):
  print('You won the game')
else:
  print('You lose the game')
