def isSelfDividingNum(num):
    s = str(num)
    for i in s:
        if i == '0':
            return False
        if num % int(i) != 0:
            return False
    return True

def selfDividingNums(left, right):
    nums = []
    for i in range(left, right + 1):
        if isSelfDividingNum(i):
            nums.append(i)
    return nums
selfDividingNums(1, 22)