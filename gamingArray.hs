{-# LANGUAGE DuplicateRecordFields, FlexibleInstances, UndecidableInstances #-}

module Main where
import Control.Monad
import Data.List
import Data.List.Split
import System.Environment
import System.IO
import Data.Bits
import Data.Maybe


gamingArray :: [Int] -> Int -> Int
gamingArray [] _ = 0
gamingArray arr max= if head arr > max then 
                        1 + gamingArray (tail arr) (head arr)
                    else 
                        gamingArray (tail arr) (max) 

winGame :: Int -> String
winGame a = if even a then 
                "ANDY"
            else
                "BOB"
main = do
    arrCount <- readLn :: IO Int
    forM_ [1..arrCount] $ \arrCount_itr -> do
        arrCountTemp <- getLine
        arrTemp <- getLine
        let arr = Data.List.map (read :: String -> Int) . words $ arrTemp
        putStrLn $ winGame $ gamingArray arr 0