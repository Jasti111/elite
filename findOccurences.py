def findOccurrences(text, first, second):
    thirds = []
    words = list(text.split())
    for i in range(len(words) - 2):
        if words[i] == first and words[i + 1] == second:
            thirds.append(words[i + 2])
    return thirds

findOccurrences("we will we will rock you", "we", "will")