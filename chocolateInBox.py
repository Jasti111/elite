def winningContainer(chocolates):
    x = xor(chocolates)
    return len([i for i in chocolates if i ^ x < i])

def xor(chocolates):
    xor = 0
    for i in chocolates:
        xor ^= i
    return xor

containers = int(input())
chocolates = list(map(int, input().split()))
print(winningContainer(chocolates))

