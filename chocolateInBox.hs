{-# LANGUAGE FlexibleInstances, UndecidableInstances, DuplicateRecordFields #-}

module Main where

import Data.List
import Data.Bits


nim_xor :: [Int] -> Int
nim_xor arr = foldl (`xor`) 0 arr

xor_array :: [Int] -> Int -> [Int]
xor_array arr b = [ x `xor` b  | x <- arr]

movesToWin :: [Int] -> [Int] -> Int
movesToWin [] _ = 0
movesToWin arr xarr | head arr > head xarr = 1 + winMoves (tail arr) (tail xarr)
                    | otherwise = winMoves (tail arr) (tail xarr)
                  
main = do
    arrCount <- readLn :: IO Int
    arrTemp <- getLine
    let arr = Data.List.map (read :: String -> Int) . words $ arrTemp
    print $ movesToWin (arr) (xor_array  arr (nim_xor arr))
