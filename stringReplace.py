def resultantString(string):
    string = string.lower()
    vowels = "aeiou"
    for i in string:
        if i in vowels:
            string = string.replace(i, "")
        else:
            string = string.replace(i, "." + i)
        string = string.replace("..", ".")
    print(string)
resultantString("aBAcAba")