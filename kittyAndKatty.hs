{-# LANGUAGE FlexibleInstances, UndecidableInstances, DuplicateRecordFields #-}

module Main where

import Control.Monad
import Data.Array
import Data.Bits
import Data.List

kittyAndKatty :: Int -> String
kittyAndKatty n | n == 1           = "Kitty"
                | (n `mod` 2) == 0 = "Kitty"
                |  otherwise       = "Katty"

main = do
    t <- readLn :: IO Int
    forM_ [1..t] $ \t_itr -> do
        n <- readLn :: IO Int
        putStrLn $ kittyAndKatty n
